import express from 'express';

const app = express();

app.get('/', function (req, res) {
  let result = 0;

  if ('a' in req.query || 'b' in req.query) {
    let a = 0,
        b = 0;

    if ('a' in req.query) {
      a = +req.query.a;
    }
    if ('b' in req.query) {
      b = +req.query.b;
    }

    result = a + b;
  }

  res.send(result.toString());
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
